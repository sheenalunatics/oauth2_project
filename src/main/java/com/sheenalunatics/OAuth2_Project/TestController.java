package com.sheenalunatics.OAuth2_Project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
	List<Product> products = new ArrayList<>(Arrays.asList(
            new Product("LIP001", "XOXO Lipstick", 1),
            new Product("BRO001", "NYX Brush On Palette", 1)
));
	
	@RequestMapping("/product")    // แบบนี้
	public List<Product> getAllProducts() {
	    return products;
	}
}
